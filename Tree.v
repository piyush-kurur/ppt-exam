(* Intervals and membership *)
Require Import Nat.
Require Import Omega.



(* Search (compare _ _ = Lt) *)

Hint Resolve nat_compare_eq.
Hint Resolve nat_compare_lt.
Hint Resolve nat_compare_gt.
Hint Resolve nat_compare_Lt_lt.
Hint Resolve nat_compare_Gt_gt.


(** The Binary tree of nats. *)

Inductive tree :=
| empty : tree
| node  : tree -> nat -> tree -> tree
.

(* Predicate that captures that a particular element is in the given tree *)
Inductive in_tree (x : nat) : tree -> Prop :=
| atRoot  : forall lt rt, in_tree x (node lt x rt)
| inLeft  : forall lt y rt, in_tree x lt -> in_tree x (node lt y rt)
| inRight : forall lt y rt, in_tree x rt -> in_tree x (node lt y rt).

Hint Constructors in_tree.
(** A binary tree with a single element *)
Definition singleton (n : nat) := node empty n empty.

(**

The [everyWhere f t] takes a predicate [f : nat -> Prop] on nats, and
asserts thatf holds on all elements of the tree.

*)
Fixpoint everyWhere (f : nat -> Prop)(t : tree) : Prop :=
  match t with
    | empty        => True
    | node lt x rt => f x /\ everyWhere f lt /\ everyWhere f rt
  end.


(** * A binary search tree.

We now build predicates for asserting a tree is a valid search
tree. We generalise the property of being a search tree to that of
being a search tree with nodes bounded appropriately.

*)



Fixpoint search_tree (t : tree) : Prop :=
  match t with
    | empty => True (* an empty tree is a search tree *)
    | node lt x rt => search_tree lt        (* the left subtree is a search tree  *)
                      /\ search_tree rt     (* the right subtree is a search tree *)
                      /\ everyWhere (fun n => n < x) lt (* elements of left sub-tree is less than [x] *)
                      /\ everyWhere (fun n => x < n) rt (* elements of right sub-tree is less than [x] *)
  end.

(* We define the insertion function *)
Fixpoint insert (t : tree) (n : nat) : tree :=
  match t with
    | empty      => singleton n
    | node l x r => match compare n x with
                      | Eq => t
                      | Gt => node l  x (insert r n)
                      | Lt => node (insert l n)   x r
                    end
  end.


(** * Prooving correctness of insert. *)

(* The crush tactics used by the proof *)

Ltac rewrite_hyp :=
     match goal with
       | [ H : ?n = _ |- context[?n] ] => rewrite H
     end.


Ltac crush_generic :=
  repeat match goal with
           | [ H : ?T |- ?T    ] => exact T
           | [ |- True         ] => constructor
           | [ |- _ /\ _       ] => constructor
           | [ |- _ /\ _ -> _  ] => intro
           | [ H : _ /\ _ |- _ ] => destruct H
           | [ |- False -> _   ] => let HypFalse := fresh "HypFalse" in intro HypFalse; inversion HypFalse
           | [ |- True  -> _   ] => let HypTrue := fresh "useless" in intro HypTrue; clear HypTrue
           | [ |- nat -> _     ] => intro
           | _ => rewrite_hyp || omega || eauto
         end.

(*

Often we have we like [H : search_tree t ] or [H : everyWhere f t] in
the premises where t is actually in a constructor form. Since both
[search_tree] and [everyWhere] is recursively defined, what is most
useful often is the recursive components of the predicate. This ltac
expression destructs the hypothesis depending on the form of t.


*)
Ltac destruct_hyp H t :=
  match t with
    | empty      => clear H      (* This predicate is useless so clear it so that we do not loop *)
    | node _ _ _ => destruct H   (* destruct this hypothesis to get the component assumptions    *)
  end.

(*

For the proofs to proceed, sub-expressions like [insert t n] needs to
be unfolded once.  This ltac expression is for that.

 *)


Ltac unfold_fold f t :=
  match t with
    | empty      => unfold f
    | node _ _ _ => unfold f ; fold f
  end.

Ltac crush :=
  repeat (crush_generic; match goal with
                           | [ |- tree -> _ ]     => (let t := fresh "t" in intro t; induction t)
                           | [ |- search_tree _ -> _ ]      => intro
                           | [ H : search_tree  ?X   |- _ ] => destruct_hyp H X
                           | [ H : in_tree _ ?X      |- _ ] => destruct_hyp H X
                           | [ H : everyWhere _ ?X  |- _  ] => destruct_hyp H X
                           | [ |- everyWhere _ ?X  ] => unfold_fold everyWhere X
                           | [ |- search_tree ?X   ] => unfold_fold search_tree X
                           | [ |- context[insert  ?X _ ] ]  => unfold_fold insert X
                           | [ |- context[singleton ?X ] ]  => unfold singleton
                           | [ |- context[?n ?= ?n0]] => (let comp := fresh "hypComp" in remember  (n ?= n0) as comp; destruct comp)
                           | [ H : Eq = (?x ?= ?y) |- _ ] => (assert (x = y) by eauto; clear H)
                           | [ H : Lt = (?x ?= ?y) |- _ ] => (assert (x < y) by eauto; clear H)
                           | [ H : Gt = (?x ?= ?y) |- _ ] => assert (x > y) by eauto; clear H
                         end).


Lemma everyWhere_insert : forall f x t, f x /\ everyWhere f t -> everyWhere f (insert t x).
  intro; crush.
Qed.

Hint Resolve everyWhere_insert.

Theorem insert_preserves_search_property : forall t n,  search_tree t -> search_tree (insert t n).
Proof.
  crush.
Qed.

Theorem insert_actually_inserts : forall t n, in_tree n (insert t n).
  crush.
Qed.
